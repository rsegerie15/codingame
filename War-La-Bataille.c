#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

char J[14]={'2', '3', '4', '5', '6', '7', '8', '9', '1', 'J', 'Q', 'K', 'A'};

/////////////////////////////////  File  /////////////////////////////////
typedef struct element element;
struct element{ char c[4]; element *suivant; };
typedef struct FILE File;
struct FILE{ element *premier; };

File *initialiser(){   
    File *file = malloc(sizeof(*file)); file->premier = NULL;
    return file; }
////////////////////////////////////////////////////////////////////////

int indice(char J[1001], char c, int n){
    int indice;
    for (int i=0; i < n; i++){
      if(J[i]==c) indice=i; 
    } return indice; }

int taille(File *file){
    int i=0; element *elementActuel = file->premier;
    if(file->premier!=NULL) while (elementActuel!= NULL){
    elementActuel = elementActuel->suivant; i++;
 } return i;
}

void relier(File *file, File *f2){
    int i=0; element *elementActuel = file->premier;
 if(file->premier!=NULL) while (elementActuel->suivant!= NULL){
     elementActuel = elementActuel->suivant; i++; }

 if(file->premier!=NULL){ elementActuel->suivant=f2->premier;
 }else{ file->premier=f2->premier; }
}

void enfiler(File *file, char * nvNombre){
    element *nouveau = malloc(sizeof(*nouveau));
    if (file == NULL || nouveau == NULL) exit(EXIT_FAILURE);

    strcpy(nouveau->c,nvNombre); nouveau->suivant = NULL;
    if (file->premier != NULL){ element *elementActuel = file->premier; 
      while (elementActuel->suivant != NULL){
            elementActuel = elementActuel->suivant; }
            elementActuel->suivant = nouveau;
    }else{ file->premier = nouveau; }
}

void defiler(File *file){
    if (file == NULL) exit(EXIT_FAILURE);  
    if (file->premier != NULL){ element *elementDefile = file->premier;
        file->premier = elementDefile->suivant; free(elementDefile); } 
}

////////////////////////////////////////////////////////////////////////

int main(){
    int MANCHES;
    int n; int m;
    scanf("%d", &n);
    File *F1 = initialiser();   File *F2 = initialiser();
    File *D1 = initialiser();   File *D2 = initialiser();
    for (int i = 0; i < n; i++) { char p1[4];
        scanf("%s", p1); enfiler(F1,p1);
    }   scanf("%d", &m);
    for (int i = 0; i < m; i++) { char p2[4];
        scanf("%s", p2); enfiler(F2,p2);
    }

    bool eq = false;  bool stop = false;  bool D = false;
    int T1 = m ; int T2 = n ;

while(!eq && !stop){
        char pc1[4]; char pc2[4];
        strcpy(pc1, F1->premier->c);
        strcpy(pc2, F2->premier->c);
        defiler(F1); defiler(F2);
        int ind1 = indice(J,pc1[0],14);
        int ind2 = indice(J,pc2[0],14);

        if(ind1 > ind2){
            if (D) relier(F1,D1);
            enfiler(F1,pc1); 
            if (D) relier(F1,D2);
            enfiler(F1,pc2); MANCHES++; 
            D=false;
            D1=initialiser(); D2=initialiser();

        }else if (ind2 > ind1){
            if (D) relier(F2,D1);
            enfiler(F2,pc1); 
            if (D) relier(F2,D2);
            enfiler(F2,pc2);
            MANCHES++; D=false;
            D1=initialiser(); D2=initialiser();
        }else{
            if(T1 < 4 || T2 < 4){
                eq=true;
            }else{  
               for(int i=0; i<3; i++){
                   enfiler(D1,pc1); strcpy(pc1,F1->premier->c);
                   defiler(F1);
                   enfiler(D2,pc2); strcpy(pc2,F2->premier->c);
                   defiler(F2);
               }   enfiler(D1,pc1); enfiler(D2,pc2); D=true;
            }
        } T1=taille(F1);   T2=taille(F2);
        if(T1==0 || T2==0) stop=true;
}
    eq==true ? printf("PAT") : 0;
    if (!eq) taille(F1) > 0 ? printf("1 ") : printf("2 ");
    if (!eq) printf("%d", MANCHES);  
    free(F1); free(F2);    free(D1); free(D2);
    return 0;
}
